### Setting up

This project is done using ReactJS and tested under Cypress. npm is required to run this project; once located on the main project folder, install all dependencies with 

```
$ npm install
```

Run the local server on a terminal under 
```
$ npm start
```
This should open a browser window to localhost:3000

### Testing

To run tests using Cypress, run the following command:

```
npm run cypress:open
```

This opens the Cypress testing suite. Afterwards, open the App.spec.js file to execute the tests.