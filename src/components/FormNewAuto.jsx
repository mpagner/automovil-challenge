import React, { Component } from 'react'
import '../css/addautoform.css'
import { FormErrors } from './FormErrors';

export default class FormNewAuto extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nombre: '',
            color: '',
            patente: '',
            formErrors: { nombre: '', color: '', patente: '' },
            hasError: false
        }
        this.handleInput = this.handleInput.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.validateAddAuto = this.validateAddAuto.bind(this);
        this.validateField = this.validateField.bind(this);
    }

    handleInput(e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({
            [name]: value
        })
    }

    validateNombre(nombre) {
        return nombre.match(/^[a-zA-Z0-9_.-\s]*$/) && nombre.length < 30 && nombre.length > 0;
    }

    validatePatente(patente) {
        let format1 = /^[a-zA-Z]{3}[0-9]{3}?$/;
        let format2 = /^[a-zA-Z]{2}[0-9]{3}[a-zA-Z]{2}?$/;
        return (patente.match(format1) || patente.match(format2))
    }

    validateColor(color) {
        return color.length <= 20 && color.length > 0 && color.match(/^[a-zA-Z]*$/)
    }

    validateField(fieldName, value) {
        let formErrors = this.state.formErrors;
        let { nombre, patente, color } = this.state;
        let hasError = this.state;


        formErrors.nombre = this.validateNombre(nombre) ? '' : ' es inválido';

        formErrors.patente = this.validatePatente(patente) ? '' : ' debe estar en formato ABC123 ó AB123CD.';

        formErrors.color = this.validateColor(color) ? '' : ' no es válido';

        hasError = formErrors.nombre || formErrors.patente || formErrors.color || false

        this.setState({
            formErrors: formErrors,
            hasError: hasError
        }, () => {
            this.validateAddAuto()
        });
    }


    validateAddAuto() {
        if (this.state.hasError) {
            return
        } else {
            this.props.addAuto(this.state)
            this.setState({
                nombre: '',
                color: '',
                patente: '',
                hasError: false,
                formErrors: {
                    nombre: '',
                    color: '',
                    patente: ''
                }
            })
        }
    }

    handleSubmit(event) {
        event.preventDefault()
        this.validateField()
    }

    render() {
        return (
            <form className="add-auto-form" onSubmit={this.handleSubmit}>
                <input type="text" name='nombre' className='nombre'
                    placeholder='Nombre del auto'
                    value={this.state.nombre}
                    onChange={this.handleInput}
                />

                <input type="text" name='color' className="color"
                    placeholder='Color'
                    value={this.state.color}
                    onChange={this.handleInput}
                />

                <input type="text" name='patente' className="patente"
                    placeholder='Patente'
                    value={this.state.patente}
                    onChange={this.handleInput}
                />

                <button type='submit'>Agregar</button>

                <FormErrors formErrors={this.state.formErrors} />
            </form>
        )
    }
}
