import React, { Component } from 'react'
import Auto from './Auto'
import '../css/listcontainer.css'
import _ from 'lodash';
import { faXmarkCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default class ListContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSearchEnabled: false,
      searchPattern: ''
    }
    this.handleSearchInput = this.handleSearchInput.bind(this)
    this.deleteSearch = this.deleteSearch.bind(this)
  }

  handleSearchInput(event) {
    if (this.state.searchPattern === '') {
      this.setState({
        isSearchEnabled: true
      })
    }
    this.setState({
      searchPattern: event.target.value
    })


  }

  deleteSearch() {
    this.setState({
      isSearchEnabled: false,
      searchPattern: ''
    })
  }

  renderAutos() {
    const pattern = this.state.searchPattern.toString().toUpperCase()
    return (!this.state.isSearchEnabled) ? (this.props.list
      .reverse()
      .map(auto => (
        <Auto
          attrs={auto}
          deleteAuto={this.props.deleteAuto}
          key={auto.patente}
        />
      )))
      :
      _.filter(this.props.list, function (item) {
          return _.startsWith(item.patente, pattern)
        })
      .reverse()
        .map(auto => (
          <Auto
            attrs={auto}
            deleteAuto={this.props.deleteAuto}
            key={auto.patente}
          />
        ))
  }

  render() {
    return (
      <div className='main-container'>
        {this.props.list.length &&
          <form className='search-bar'>
            <input placeholder='Filtrar por patente'
              name='searchPattern'
              value={this.state.searchPattern}
              onChange={this.handleSearchInput}
            />
            <span className='removeClick'>
              <FontAwesomeIcon className='faicons' icon={faXmarkCircle}
                onClick={this.deleteSearch}
              />
            </span>
          </form>
        }

        <div className='list-container'>

        </div>
        <div className='list'>
          {this.renderAutos()}
        </div>
      </div>
    )
  }
}
