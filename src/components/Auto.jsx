import React, { Component } from 'react'
import '../css/autoitem.css'
import auto from '../auto-icon.png'
import { faTrash } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default class Auto extends Component {

  render() {
    return (
      <li>
        <div className='card'>
          <div className='info'>
            <h1>{this.props.attrs.nombre}</h1>

            <p>Color: {this.props.attrs.color}</p>
            <p>Patente: {this.props.attrs.patente}</p>
          </div>
          <div className='buttons'>
            <span>
              <FontAwesomeIcon className='faicons' icon={faTrash} color='black'
                onClick={() => {
                  if (window.confirm('¿Seguro que querés borrar este item?')) this.props.deleteAuto(this.props.attrs.patente)
                }}
              />
            </span>
          </div>
          <div className='auto-image'>
            <img src={auto} alt="auto"/>
          </div>
        </div>
      </li >
    )
  }
}
