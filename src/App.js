import './App.css';
import ListContainer from './components/ListContainer';
import FormNewAuto from './components/FormNewAuto';

import React, { Component } from 'react'
import _ from 'lodash';

import { library } from '@fortawesome/fontawesome-svg-core'
import { faTrash } from '@fortawesome/free-solid-svg-icons';

library.add(faTrash);

export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      list: []
    }

    this.addAuto = this.addAuto.bind(this);
    this.deleteAuto = this.deleteAuto.bind(this)
  }


  addAuto(attrs) {
    let newAuto = attrs;
    newAuto.patente = newAuto.patente.toUpperCase()
    if (_.find(this.state.list, { 'patente': newAuto.patente })) {
      window.alert('Esta patente ya está registrada')
    } else {
      this.setState({
        list: [...this.state.list, newAuto]
      });
    }
  }

  deleteAuto(key) {
    let filteredAutos = this.state.list.filter(auto => auto.patente !== key);
    this.setState({
      list: filteredAutos
    })
  }



  render() {
    return (
      <div className="App">
 
        <h1>Auto Challenge</h1>
        <div className='contents'>
          <FormNewAuto addAuto={this.addAuto} />
          <ListContainer list={this.state.list} deleteAuto={this.deleteAuto} />
        </div>
      </div>
    )
  }
}