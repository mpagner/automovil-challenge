describe('Automovile adding test', () => {
    it('Creates three vehicles and prevents license reps.', () => {
        cy.visit('localhost:3000')
        cy.get('.nombre').type('Renault Clio')
        cy.get('.color').type('Verde')
        cy.get('.patente').type('ABC123{enter}')

        cy.get('.nombre').type('Fiat Palio')
        cy.get('.color').type('Verde')
        cy.get('.patente').type('PAC536{enter}')

        cy.get('.nombre').type('Renault Clio')
        cy.get('.color').type('Azul')
        cy.get('.patente').type('AAC683{enter}')

        cy.get('li').should('have.length', 3)

        cy.get('.nombre').type('Camaro SS')
        cy.get('.color').type('Amarillo')
        cy.get('.patente').type('AAC683{enter}')

        cy.get('li').should('have.length', 3)
    })
})

describe('Automovile deletion test', () => {
    it('Creates three vehicles, then deletes all.', () => {
        cy.visit('localhost:3000')
        cy.get('.nombre').type('Renault Clio')
        cy.get('.color').type('Verde')
        cy.get('.patente').type('ABC123{enter}')

        cy.get('.nombre').type('Fiat Palio')
        cy.get('.color').type('Verde')
        cy.get('.patente').type('PAC536{enter}')

        cy.get('.nombre').type('Renault Clio')
        cy.get('.color').type('Azul')
        cy.get('.patente').type('AAC683{enter}')

        cy.get('li').should('have.length', 3)

        cy.get('.faicons').click({multiple: true})

        cy.get('li').should('not.exist')
    })
})

describe('Licence plate validation test', () => {
    it('Attempts different parameters for a licence plate.', () => {
        cy.visit('localhost:3000')
        cy.get('.nombre').type('Renault Clio')
        cy.get('.color').type('Verde')
        cy.get('.patente').type('AB123{enter}')
        cy.get('.formErrors p').should('contain', 'debe estar en formato')

        cy.get('.patente').clear().type('ABC1234{enter}')
        cy.get('.formErrors p').should('contain', 'debe estar en formato')

        cy.get('.patente').clear().type('ABC!23{enter}')
        cy.get('.formErrors p').should('contain', 'debe estar en formato')

        cy.get('.patente').clear().type('a{enter}')
        cy.get('.formErrors p').should('contain', 'debe estar en formato')

        cy.get('.patente').clear().type('{enter}')
        cy.get('.formErrors p').should('contain', 'debe estar en formato')

        cy.get('.patente').clear().type('abc123{enter}')
        cy.get('.list .card .info > :nth-child(3)').should('contain', 'ABC123')

        cy.get('.nombre').type('Renault Clio')
        cy.get('.color').type('Azul')
        cy.get('.patente').type('Ab123cD{enter}')
        cy.get('.list > :nth-child(1) .card .info > :nth-child(3)').should('contain', 'AB123CD')
        cy.get('.list > :nth-child(2) .card .info > :nth-child(3)').should('contain', 'ABC123')

    })
})